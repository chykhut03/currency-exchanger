import datetime
from decimal import Decimal

import bs4
import requests

exchange_url = 'https://minfin.com.ua/ua/currency/banks/{currency}/{date}/'


def get_exchange_rates(date, currency='usd'):
    data = {}
    url = exchange_url.format(currency=currency, date=date)
    html = requests.get(url).text

    bs4obj = bs4.BeautifulSoup(html, 'html.parser')
    rows = bs4obj.find_all('td', attrs={'class': 'mfcur-table-bankname'})
    for row in rows:
        bank_link = row.find('a')
        if not bank_link:
            continue
        bank_name = bank_link.text.strip()
        prices = row.attrs['data-title'].split('/')
        if len(prices) != 2:
            prices = row.attrs['data-card'].split('/')
        buy_price = Decimal(prices[0])
        sell_price = Decimal(prices[1])
        data[bank_name] = (sell_price + buy_price) / 2
    return data


def get_exchange_rates_history(start_date, end_date, currency='usd'):
    data = {}
    date = start_date
    while date <= end_date:
        data[date] = get_exchange_rates(date, currency)
        date += datetime.timedelta(days=1)
    return data


def show_diagram(start_date, end_date, currency='usd'):
    data = get_exchange_rates_history(start_date, end_date, currency)
    import matplotlib.pyplot as plt
    import pandas as pd
    df = pd.DataFrame(data)

    for r in df.iloc:
        plt.plot(r, label=r.name)

    plt.xticks(ticks=df.columns, rotation=35, labels=df.columns)
    plt.xlabel('Date', fontsize=12)
    plt.ylabel('Exchange Rates', fontsize=12)
    plt.legend(bbox_to_anchor=(1.04, 0), loc="lower left", borderaxespad=0)

    plt.show()
