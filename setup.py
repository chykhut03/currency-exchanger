from setuptools import setup, find_packages

VERSION = '0.0.3'

with open("README.md", "r") as f:
    long_description = f.read()

DESCRIPTION = ('A small library for getting information on exchange rates the NBU on the selected date')

setup(
    name="chyhut-currency-exchanger",
    version=VERSION,
    author="Mariia Chykhut",
    author_email="chyhut@gmail.com",
    license='MIT',
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type="text/markdown",

    packages=find_packages(),
    install_requires=['requests', 'bs4', 'pandas', 'matplotlib'],

    keywords=['python', 'currency exchanger', 'bank'],
    classifiers=[
        "Intended Audience :: Education",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    project_urls={
        'Source': 'https://gitlab.com/chykhut03/chyhut-currency-exchanger',
    },
)