![PyPI - Python Version](https://img.shields.io/pypi/pyversions/privat_exchange_rates?style=for-the-badge)

**Official Repo:** https://gitlab.com/chykhut03/currency-exchanger

It uses the following libraries:

- [Requests](https://pypi.org/project/requests/) for requests to exchange rates

# Quick Install / Usage

```bash
pip install chyhut-currency-exchanger
```

```python
from datetime import date

from currency_exchanger.currency import get_exchange_rates

print(get_exchange_rates(date(2022, 2, 2)))

# Output:
# {'OTP Bank': Decimal('28.325'),
#  'Європромбанк': Decimal('28.375'),
#  'Ідея Банк': Decimal('28.300'),
#  'Індустріалбанк': Decimal('28.350'),
#  'А-Банк': Decimal('28.300'),
#  'Агропросперіс Банк': Decimal('28.340'),
#  'БТА Банк': Decimal('28.375'),
#  'Банк 3/4': Decimal('28.400'),
#  'Банк Кредит Дніпро': Decimal('28.335'),
#  'Грант': Decimal('28.350'),
#  'Міжнародний Інвестиційний Банк': Decimal('28.375'),
#  'ОКСІ Банк': Decimal('28.335'),
#  'Ощадбанк': Decimal('28.380'),
#  'ПУМБ': Decimal('28.300'),
#  'Перший інвестиційний Банк': Decimal('28.305')}
```